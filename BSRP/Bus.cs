﻿using System;
using System.Collections.Generic;

namespace BSRP
{
    /// <summary>
    /// Bus is a class that represent a single bus for bikes collection/distributing
    /// </summary>
    class Bus
    {
        Queue<int> stops;           /* This queue represents the sequence of stops */
        Queue<int> bikes;           /* bikes[i] represents the number of bikes picked up at stop i */
        int[] b;                    /* Local copy of demand array */

        int initialBikes = -1;  /* Initial bikes for the bus. -1 is a dummy value */
        int currentBikes = -1;  /* Bikes currently present in the bus */
        int currentPosition;    /* Bus current position */

        int busCost;            /* Cumulative cost for the bus */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b">The array of nodes' bikes requests (local copy)</param>
        /// <param name = "minD">The matrix of minimum costs</param>
        public Bus(int[] b)
        {
            stops = new Queue<int>();
            bikes = new Queue<int>();
            this.b = b;

            busCost = 0;
            currentPosition = 0;
        }

        /// <summary>
        /// Add a stop for the current bus and move it.
        /// </summary>
        /// <param name="stopNode">The next node in which it's going to stop</param>
        public void MoveTo(int stopNode)
        {
            stops.Enqueue(stopNode);                /* First, we add the node to the records */
            bikes.Enqueue(b[stopNode]);             /* Then, we update the number of bikes picked up at each stop */
            currentBikes += b[stopNode];            /* ..and the current bikes available */
            b[stopNode] -= b[stopNode];             /* Finally, we update the nodes requests */

            busCost += Globals.d[currentPosition, stopNode];

            currentPosition = stopNode;             /* Update the current position to the latest stop */
        }

        /// <summary>
        /// Randomly generate the number of initial bikes
        /// </summary>
        /// <param name="rand">The random numbers generator</param>
        /// <param name="firstStopRequest">The bikes pick-up request for the first stop of the bus</param>
        public void GenerateInitialBikes(Random rand, int firstStopRequest) 
        {
            if (firstStopRequest < 0) initialBikes = (-firstStopRequest > Globals.Q) ? Globals.Q : rand.Next(-firstStopRequest, Globals.Q + 1);
            else initialBikes = (firstStopRequest > Globals.Q) ? 0 : rand.Next(0, Globals.Q - firstStopRequest + 1);
            currentBikes = initialBikes;
        }

        /// <summary>
        /// Set the number of initial bikes
        /// </summary>
        /// <param name="initialBikes">The initial bikes number</param>
        public void SetInitialBikes(int initialBikes)
        {
            this.initialBikes = initialBikes;
            this.currentBikes = initialBikes;
        }
       
        /// <summary>
        /// Output bus stops & bikes picked-up accordingly to output format request
        /// </summary>
        /// <returns>The formatted output</returns>
        public string GetOutput()
        {
            string busResult = "";
            int[] nodes = stops.ToArray();
            int[] bikes = this.bikes.ToArray();
            
            /* We use nodes.Length - 1 as limit to avoid the depot to be shown in the output */
            for(int i = 0; i < nodes.Length - 1; i++)
            {
                /* Format: Ni1 Bi1, Ni2 Bi2, ... 
                   Where Nij = ID of the j-th stop for the current i-th bus
                         Bij = Bikes picked up from the j-th stop for the current i-th bus */
                busResult += string.Format("{0}{1} {2}", i == 0 ? "" : ", ", nodes[i], bikes[i]);       
            }
            return busResult;
        }
    
        /// <summary>
        /// Get the number of stops for the current bus
        /// </summary>
        /// <returns>The number of stops</returns>
        public int GetStopsCount()
        {
            return stops.Count;
        }

        /// <summary>
        /// Get the currently available bikes of the bus
        /// </summary>
        /// <returns>The number of bikes</returns>
        public int GetCurrentBikes()
        {
            return currentBikes;
        }

        /// <summary>
        /// Get the current free room of the bus
        /// </summary>
        /// <returns>The free room</returns>
        public int GetCurrentFreeRoom()
        {
            return Globals.Q - currentBikes;
        }

        /// <summary>
        /// Get the current cumulative bus cost
        /// </summary>
        /// <returns>The cumulative bus cost</returns>
        public int GetBusCost()
        {
            return busCost;
        }

        /// <summary>
        /// Get the current node for the bus
        /// </summary>
        /// <returns>The current node</returns>
        public int GetCurrentPosition()
        {
            return currentPosition;
        }

    }
}
