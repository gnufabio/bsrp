﻿using System;
using System.Threading;

namespace BSRP
{
    /// <summary>
    /// A solvers which solves a certain number of <see cref="BikeSharingProblem"/> and finds the best solution
    /// </summary>
    class SolverInstance
    {
        private BikeSharingProblem bestSolution;    /* The best solution found by the thread */
        private BikeSharingProblem problem;         /* The current problem */

        int minCriticalCost = -1;

        int lowerCost = -1;                     /* The local lower cost */
        int mainThreadLowerCost;                /* The minimum cost found by every thread */
        
        public Thread THREAD;                   /* The THREAD object */
        private int ID;                         /* The ID of the THREAD */

        int randLevel;

        ReadDelegate ReadLowerCost;             /* The delegate to read the mainThreadLowerCost */
        WriteDelegate WriteLowerCost;           /* The delegate to write the mainThreadLowerCost */
        PermissionDelegate CanWrite;            /* The delegate to check whether the current thread can write mainThreadLowerCost */
        TokenUpdateDelegate TokenToNext;        /* The delegate to give the token to the next thread */

        Random rand;                            /* The random number generator */

        /// <summary>
        /// Inits the SolverInstance
        /// </summary>
        /// <param name="read">The <see cref="ReadDelegate"/></param>
        /// <param name="write">The <see cref="WriteDelegate"/></param>
        /// <param name="permission">The <see cref="PermissionDelegate"/></param>
        /// <param name="token">The <see cref="TokenUpdateDelegate"></see></param>
        /// <param name="threadID">The ID of this THREAD</param>
        /// <param name="randLevel">The randomnes level</param>
        public SolverInstance(ReadDelegate read, WriteDelegate write, PermissionDelegate permission, TokenUpdateDelegate token, int threadID, int randLevel = 0)
        {
            THREAD = new Thread(LaunchInstances);
            ReadLowerCost = read;
            WriteLowerCost = write;
            CanWrite = permission;
            TokenToNext = token;
            ID = threadID;
            rand = ThreadLocalRandom.NewRandom();
            this.randLevel = randLevel;
        }

        /// <summary>
        /// Start the thread
        /// </summary>
        public void Start()
        {
            THREAD.Start();
        }

        /// <summary>
        /// Compute the solution of all <paramref name="REPEAT"/> istances
        /// </summary>
        private void LaunchInstances()
        {
            for(int firstNode = 1; firstNode < Globals.N; firstNode++)
            {
                int lowerLimit = (Globals.b[firstNode] >= 0) ? 0 : -Globals.b[firstNode];
                int upperLimit = (Globals.b[firstNode] >= 0) ? Globals.Q - Globals.b[firstNode] : Globals.Q;
                int times = Globals.REPEAT / ((Globals.N - 1) * (Math.Abs(upperLimit - lowerLimit) + 1));

                for(int initialBikes = lowerLimit; initialBikes <= upperLimit; initialBikes++)
                {
                    for(int i = 0; i <= times; i++) LaunchAndCheckProblem(initialBikes, firstNode);
                }
            }

        }
        
        /// <summary>
        /// Launch and check a problem solution results
        /// </summary>
        /// <param name="initialBikes">The number of initial bikes</param>
        /// <param name="firstNode">The ID of the first node</param>
        private void LaunchAndCheckProblem(int initialBikes = -1, int firstNode = -1)
        {
            bool success;
            int currentCost;

            /* If the mainThreadLowerCost is better than actual lowerCost, update it */
            mainThreadLowerCost = ReadLowerCost();
            if(mainThreadLowerCost < lowerCost) lowerCost = mainThreadLowerCost;

            /* Solve a new BikeSharingProblem */
            problem = new BikeSharingProblem(rand, lowerCost, minCriticalCost, initialBikes, firstNode, randLevel);
            success = problem.FindFeasibleSolution();
            currentCost = problem.CalculateTotalCost();

            int criticalCost = problem.GetCostAtCriticalMove();
            if(minCriticalCost == -1 || (criticalCost != -1 && criticalCost < minCriticalCost))
            {
                minCriticalCost = criticalCost;
            }

            /* If we have improved the score */
            if(success)
            {
                
                if(currentCost < lowerCost || lowerCost == -1) {
                    /* Update local lowerCost, and, if possible,  */
                    mainThreadLowerCost = ReadLowerCost();
                    lowerCost = currentCost;
                    bestSolution = problem;
                
                    if(CanWrite(ID))
                    {
                        /* If we have the token, improve the solution or give the token to another thread */
                        if(mainThreadLowerCost == -1 || lowerCost < mainThreadLowerCost) WriteLowerCost(ID, lowerCost);
                        else TokenToNext();
                    }
                }
            }
        }

        /// <summary>
        /// Get the best solution found by this solver
        /// </summary>
        /// <returns>The best solution</returns>
        public BikeSharingProblem GetBestSolution()
        {
            return bestSolution;
        }
        
    }
}
