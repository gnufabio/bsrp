﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSRP
{
    public delegate int ReadDelegate();
    public delegate void WriteDelegate(int ID, int lowerCost);
    public delegate bool PermissionDelegate(int threadID);
    public delegate void TokenUpdateDelegate();

    /// <summary>
    /// Global variables seen by every class
    /// </summary>
    static class Globals
    {
        public static int REPEAT;                                 /* Number of instance for each solver */
        public const int REPEAT_BIG = 1720000;                    /* Number of instances for big problems, reduced because of time limits */
        public const int REPEAT_SMALL_MEDIUM = 5220000;           /* Number of instance for small & medium problems */
        public const int SIZE_THRESHOLD = 60;                     /* Threshold between BIG and MEDIUM problems */

        public const double CRITICAL_RATIO = 1.6;                 /* The percentage to determine which is the critical move */
        public static int CRITICAL_MOVE = 15;                     /* The number of moves after which we decide to continue the current solution or discharge it */
            
        public static string FILENAME = "50Boston16.txt";         /* Filename of data file */
        public static int THREADS;                                /* Numbers of thread to be launched */

        /* THE FOLLOWING VARIABLES SHOULD NOT BE WRITED EXCEPT WHEN WE INITIALIZE THEM */

        public static int N;                                      /* Number of nodes */
        public static int Q;                                      /* Capacity of each bus */
        public static int[,] d;                                   /* Matrix of distances */
        public static int[] b;                                    /* Vector of demands */
    }
}
