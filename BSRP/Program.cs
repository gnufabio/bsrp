﻿using System;
using System.Globalization;
using System.Diagnostics;
using System.IO;

namespace BSRP
{
    class Program
    {
        static bool printToConsole = false;                /* Debug flag */

        static int lowerEverCost = -1;
        static int tokenID = 0;

        static void Main(string[] args)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            /* Number of threads is conventionally equals to the number of logical CPUs */
            Globals.THREADS = Environment.ProcessorCount;
            SolverInstance [] solvers = new SolverInstance[Globals.THREADS];
            BikeSharingProblem bestSolution = null;

            /* If a filename is specified from command line, use it as data source */
            if (args.Length >= 1) Globals.FILENAME = args[0];
            if(Debugger.IsAttached || (args.Length >= 2 && Boolean.Parse(args[1])) || args.Length == 0) printToConsole = true;

            /* Read the input data from the file and save it in the variables */
            ReadData(Globals.FILENAME);

            Globals.REPEAT = (Globals.N >= Globals.SIZE_THRESHOLD) ? Globals.REPEAT_BIG : Globals.REPEAT_SMALL_MEDIUM;

            /* Init each solver thread and start it */
            for(int i = 0; i < Globals.THREADS; i++)
            {
                solvers[i] = new SolverInstance(GetMainLowerCost, SetLowerCost, CanWrite, TokenToNextThread, i, i % 4);
                solvers[i].Start();
            }

            /* Wait untill each solver has finished */
            for(int i = 0; i < Globals.THREADS; i++)
            {
                solvers[i].THREAD.Join();
            }

            stopwatch.Stop();
            /* Find the best solution */
            bestSolution = GetTheSolution(solvers);

            if(printToConsole)
            {
                bestSolution.Output();
                Console.WriteLine("Total cost: {0}; RandLevel {2}. Result in {1}s.", bestSolution.CalculateTotalCost(), stopwatch.ElapsedMilliseconds / 1000, bestSolution.GetRandomnessLevel());
                Console.Beep();
                if(Debugger.IsAttached) Console.ReadLine();
            }
            else
            {
                bestSolution.Output(GetOutputFilename());
            }
        }
        

        /// <summary>
        /// Read the input data from the file
        /// </summary>
        /// <param name="filename">Filename of input file</param>
        public static void ReadData(string filename)
        {
            string[] lines = System.IO.File.ReadAllLines(filename);
            Globals.N = int.Parse(lines[0]);

            Globals.b = new int[Globals.N];
            Globals.d = new int[Globals.N, Globals.N];

            string[] s = lines[1].Split('\t');
            for (int i = 0; i < Globals.N; i++)
            {
                Globals.b[i] = int.Parse(s[i]);
            }

            Globals.Q = int.Parse(lines[2]);

            for (int i = 0; i < Globals.N; i++)
            {
                s = lines[i + 3].Split('\t');
                for (int j = 0; j < Globals.N; j++)
                {
                    Globals.d[i, j] = (int)decimal.Parse(s[j], NumberStyles.Float, CultureInfo.GetCultureInfo("en-US"));
                }
            }
        }

        /// <summary>
        /// Find the best solution among the solution found by solvers
        /// </summary>
        /// <param name="solvers">The array of solvers</param>
        /// <returns>The best solution</returns>
        public static BikeSharingProblem GetTheSolution(SolverInstance[] solvers)
        {
            BikeSharingProblem TheSolution = null;
            foreach(SolverInstance solver in solvers)
            {
                if (TheSolution == null) TheSolution = solver.GetBestSolution();
                else TheSolution = (solver.GetBestSolution() != null && solver.GetBestSolution().CalculateTotalCost() < TheSolution.CalculateTotalCost()) 
                                        ? solver.GetBestSolution() : TheSolution;
            }
            return TheSolution;
        }

        /// <summary>
        /// Get the best result obtained yet
        /// </summary>
        /// <returns>The minimum cost</returns>
        public static int GetMainLowerCost()
        {
            return lowerEverCost;
        }
        
        /// <summary>
        /// Update the best result ever obtained by the instances
        /// </summary>
        /// <param name="threadID">The ID of the THREAD</param>
        /// <param name="lowerCost">The minimum cost</param>
        public static void SetLowerCost(int threadID, int lowerCost)
        {
            if(threadID == tokenID)
            {
                if(lowerCost < lowerEverCost || lowerEverCost == -1) lowerEverCost = lowerCost;
            }
            else return;

            /* Give the token to another thread */
            TokenToNextThread();
        }

        /// <summary>
        /// Check whether a THREAD can write the score obtained or not
        /// </summary>
        /// <param name="threadID">The ID of the THREAD</param>
        /// <returns></returns>
        public static bool CanWrite(int threadID)
        {
            return threadID == tokenID;
        }

        /// <summary>
        /// Update the thread with token
        /// </summary>
        public static void TokenToNextThread()
        {
            if(tokenID == Globals.THREADS - 1) tokenID = 0;
            else if(tokenID < Globals.THREADS - 1) tokenID++;
        }

        /// <summary>
        /// Get the output filename
        /// </summary>
        /// <returns>The output filename</returns>
        public static string GetOutputFilename()
        {
            string extension = Path.GetExtension(Globals.FILENAME);
            return Globals.FILENAME.Replace(extension, "_sol" + extension);
        }
    }
    
}