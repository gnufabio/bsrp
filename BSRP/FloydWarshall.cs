﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSRP
{
    /// <summary>
    /// This class implements the Floyd-Warshall algorithm to find the minimum path between each couple of nodes.
    /// </summary>
    class FloydWarshall
    {
        Boolean negativeCycle = false;
        int N;                              /* The number of nodes */
        int[,] D;                           /* The matrix of minimum costs */
        int[,] P;                           /* The matrix of predecessors */

        /// <summary>
        /// Init the Floyd-Warshall algorithm istance
        /// </summary>
        /// <param name="D">The matrix of distances</param>
        /// <param name="N">The number of nodes</param>
        public FloydWarshall(int [,] D, int N)
        {
            this.D = D;
            this.N = N;
            InitPredecessorsMatrix();
        }

        /// <summary>
        /// Calculate the minimum distance matrix and the predecessors matrix
        /// </summary>
        public void compute()
        {
            for(int k = 0; k < N; k++)
            {
                for (int i = 0; i < N; i++)
                {
                    if (i == k) continue;
                    for (int j = 0; j < N; j++)
                    {
                        if (D[k,j] == int.MaxValue || D[j,i] == int.MaxValue || D[k,j] == 0 || D[j,i] == 0) continue;
                        if (D[k,i] > D[k,j] + D[j,i])
                        {
                            D[k,i] = D[k,j] + D[j,i];
                            P[k, i] = P[j, i];
                        }
                    }
                }

                for(int l = 0; l < N; l++)
                {
                    if(D[l,l] < 0)
                    {
                        negativeCycle = true;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Init the matrix of predecessors
        /// </summary>
        public void InitPredecessorsMatrix()
        {
            P = new int[N, N];
            for(int i = 0; i < N; i++)
            {
                for(int j = 0; j < N; j++)
                {
                    if(D[i,j] == int.MaxValue || D[i,j] <= 0) P[i, j] = -1;
                    else P[i, j] = i;
                }
            }
        }

        /// <summary>
        /// Detects whether there are negative cycles or not
        /// </summary>
        /// <returns>True if there are negative cycles</returns>
        public bool HasNegativeCycle()
        {
            return negativeCycle;
        }

        /// <summary>
        /// Get the minimum distance matrix
        /// </summary>
        /// <returns>The minimum distance matrix</returns>
        public int [,] GetDistancesMatrix()
        {
            return D;
        }

        /// <summary>
        /// Get thhe predecessors matrix
        /// </summary>
        /// <returns>The predecessors matrix</returns>
        public int [,] GetPredecessorMatrix()
        {
            return P;
        }
    }
}
