﻿using System;

namespace BSRP
{
    class BikeSharingProblem
    {
        int[] b;                        /* Local copy of demand array */

        int lowerCost;                  /* Minimum cost ever reached */
        int currMove = 0;               /* Current move */
        int minCriticalCost = -1;       /* Minimum critical cost obtained by the solver */

        bool minimumCostOverreached;    /* This flag is turned on when the minimum cost is overreached or the critical cost is too high */

        double[,] scores;               /* Score assigned to every arc */

        Bus[] allBus;                   /* Array of all the bus */
        
        Random rand;                    /* The random numbers generator */
        int randLevel;                  /* The randomness level, which influences alpha and beta */
        int firstNode;                  /* First node to be reached by the first bus */
        int initialBikes;               /* The number of first bus initial bikes */

        /// <summary>
        /// Init a new BikeSharingProblem
        /// </summary>
        /// <param name="rand">Random generator</param>
        /// <param name="lowerCost">Minimum cost</param>
        /// <param name="minCriticalCost">Minimum critical cost obtained by the solver</param>
        /// <param name="firstNode">The first node to be reached</param>
        /// <param name="initialBikes">The initial bikes of the first bus</param>
        /// <param name="randLevel">The randomness level</param>
        public BikeSharingProblem(Random rand, int lowerCost, int minCriticalCost = -1, int initialBikes = -1, int firstNode = -1, int randLevel = 0)
        {
            scores = new double[Globals.N, Globals.N];
            this.b = (int[]) Globals.b.Clone();
            this.lowerCost = lowerCost;
            this.initialBikes = initialBikes;
            this.firstNode = firstNode;
            this.rand = rand;
            this.minCriticalCost = minCriticalCost;
            this.randLevel = (randLevel < 0) ? 0 : (randLevel > 3 ? 3 : randLevel);          /* We have just 4 random levels */

            allBus = new Bus[Globals.N];
            minimumCostOverreached = false;
        }

        /// <summary>
        /// Try to find a feasible solution for the problem
        /// </summary>
        /// <returns>true if the solution improves the cost</returns>
        public bool FindFeasibleSolution()
        {
            int currentBus = 0;
            while(!IsEveryNodeSatisfied() && !minimumCostOverreached)
            {
                allBus[currentBus] = new Bus(b);
                BusStep(ref allBus[currentBus]);
                currentBus++;
                if(minimumCostOverreached) return false;
            }
            return true;
        }

        /// <summary>
        /// This is the core of the problem solver. 
        /// Move the bus where the heuristic suggest you to go.
        /// </summary>
        /// <param name="bus">The current bus</param>
        private void BusStep(ref Bus bus)
        {
            int currPos = bus.GetCurrentPosition();
            int nextNode;
            /* If we have overreached the minimum cost, we will never improve the solution. Stop everything! */
            if(lowerCost > 0 && CalculateTotalCost() > lowerCost)
            {
                minimumCostOverreached = true;
                return;
            }
            /* If the current move is the critical one */
            if(currMove == Globals.CRITICAL_MOVE)
            {
                int currCost = CalculateTotalCost();
                if(currCost < minCriticalCost || minCriticalCost == -1)
                {
                    minCriticalCost = currCost;
                } else if(currCost > Globals.CRITICAL_RATIO * minCriticalCost)
                {
                    minimumCostOverreached = true;
                    return;
                }
            }
            if(currPos == 0 && bus.GetCurrentBikes() == -1)
            {
                /* New bus leaving the depot */
                UpdateOutgoingScore(currPos);                           /* Generate first score matrix */
                if(firstNode != -1 && initialBikes != -1)
                {
                    nextNode = firstNode;
                    bus.SetInitialBikes(initialBikes);
                    bus.MoveTo(firstNode);
                    firstNode = -1;
                    initialBikes = -1;
                }
                else
                {
                    nextNode = HeuristicFirstNode(); 
                    bus.GenerateInitialBikes(rand, b[nextNode]);    /* Generate the number of initial bikes */
                    bus.MoveTo(nextNode);                           /* Reach the first node */
                }
                currMove++;
                BusStep(ref bus);                                   /* Continue the route, looking for other nodes */
                return;
            }
            else if(currPos == 0 && bus.GetStopsCount() > 0)
            {
                /* We went back to the depot, end of the run. */
                return;
            }

            /* The bus is already going around */
            UpdateOutgoingScore(currPos);                           /* Update the score of the adjiacent nodes */
            nextNode = HeuristicNextNode(bus);                      /* Get the best node according to the heuristic */
            bus.MoveTo(nextNode);                                   /* Get to that node following the route */
            currMove++;
            BusStep(ref bus);                                       /* And go over again! */
            return;
        }

        /// <summary>
        /// Search for the highest-score node 
        /// </summary>
        /// <param name="currNode">The current node</param>
        /// <returns>The highest-score node</returns>
        public int FindMaxScoreNode(int currNode)
        {
            int maxPos = 0;
            double max = 0.0;

            for(int j = 0; j < Globals.N; j++)
            {
                if(Math.Abs(scores[currNode, j]) > max)
                {
                    maxPos = j;
                    max = Math.Abs(scores[currNode, j]);
                }
            }
            return maxPos;
        }

        /// <summary>
        /// Search for the highest-score node, under an upper bound
        /// </summary>
        /// <param name="currNode">The current node</param>
        /// <param name="bound">The upper bound</param>
        /// <returns>The highest-score node</returns>
        public int FindMaxScoreNode(int currNode, double bound)
        {
            int maxPos = 0;
            double max = 0.0;
            double current;

            for(int j = 0; j < Globals.N; j++)
            {
                current = Math.Abs(scores[currNode, j]);
                if(current > max && current < Math.Abs(bound))
                {
                    maxPos = j;
                    max = current;
                }
            }
            return maxPos;
        }

        /// <summary>
        /// Get the first node to be visited by a bus
        /// </summary>
        /// <returns>First node ID</returns>
        public int HeuristicFirstNode()
        {
            return FindMaxScoreNode(0);
        }

        /// <summary>
        /// Heuristically find the next node.
        /// A coefficient is multiplied to costs checks and sometimes the highest-score node is skipped.
        /// </summary>
        /// <param name="bus">The current bus</param>
        /// <returns>The next node to be reached</returns>
        public int HeuristicNextNode(Bus bus)
        {
            int next = -1;                                                              /* Dummy value for next node */
            int currNode = bus.GetCurrentPosition();                                    /* Current position of the bus */
            int currBikes = bus.GetCurrentBikes();                                      /* Current bikes in the bus */
            bool flag = true;
            int n = 1;

            do
            {
                /* If it's not the first time the cycle is repeated, it means that the highest-score node can be satisfied by the current
                 * bus charge. So go to a lower-score node.
                 */
                next = (next == -1) ? FindMaxScoreNode(currNode) : FindMaxScoreNode(currNode, scores[currNode, next]);
                if(IsDestinationOk(next, currBikes))
                {
                    if(rand.NextDouble() < GetBeta() / n) n++;
                    else flag = false;
                }
            } while(flag);

            /*  If it's better to go back to the depot, go to it. */
            if(next == -1 || Globals.d[currNode, next] >= (Globals.d[currNode, 0] + Globals.d[0, next]) * GetAlpha()) return 0;
            else return next;
        }

        /// <summary>
        /// Check wheter the current charge can satisfy the destination demand or not
        /// </summary>
        /// <param name="destination">The destination node</param>
        /// <param name="currCharge">The current charge</param>
        public bool IsDestinationOk(int destination, int currCharge)
        {
            if(b[destination] < 0) return currCharge >= -b[destination];
            else return Globals.Q - currCharge >= b[destination];
        }

        /// <summary>
        /// Assign a score to every node outgoing from the current position
        /// The score is proportional to its demand, and inversely proportional to the square of the arc.
        /// A proportional coefficient is multiplyed to the score to introduce randomness
        /// </summary>
        /// <param name="currPos">The current position</param>
        private void UpdateOutgoingScore(int currPos)
        {
            for(int j = 0; j < Globals.N; j++)
            {
                if(b[j] == 0 || Globals.d[currPos, j] == 0) scores[currPos, j] = 0;
                else scores[currPos, j] = (double) b[j] * GetAlpha() / (Globals.d[currPos, j] * Globals.d[currPos, j]);
            }
        }
        
        /// <summary>
        /// Check wheter all the nodes request are satisfied or not
        /// </summary>
        public bool IsEveryNodeSatisfied()
        {
            foreach(int bi in b)
            {
                if(bi != 0) return false;
            }
            return true;
        }

        /// <summary>
        /// Calculate total cost for the solution
        /// </summary>
        /// <returns>Total cost</returns>
        public int CalculateTotalCost()
        {
            int cost = 0;
            for(int i = 0; i < Globals.N; i++)
            {
                if(allBus[i] == null) return cost;
                else cost += allBus[i].GetBusCost();
            }
            return cost;
        }

        /// <summary>
        /// Print out the solution output for all bus accordingly to the output format specification
        /// </summary>
        /// <param name="filename">The output filename</param>
        public void Output(string filename = null)
        {
            string output = "";
            for(int i = 0; i < allBus.Length; i++)
            {
                if(allBus[i] != null) output += allBus[i].GetOutput();
                else break;
                output += Environment.NewLine;
            }
            if(filename == null)  Console.WriteLine(output);
            else System.IO.File.WriteAllText(@filename, output);
        }

        /// <summary>
        /// Get the alpha random coefficient
        /// </summary>
        /// <returns>Alpha</returns>
        private double GetAlpha()
        {
            return rand.Next(400 + (randLevel + 1) * 100, 1001) / 1000.0;
        }

        /// <summary>
        /// Get the beta random coefficient
        /// </summary>
        /// <returns>Beta</returns>
        private double GetBeta()
        {
            switch(randLevel)
            {
                case 0:
                    return rand.Next(175, 401) / 1000.0;
                case 1:
                    return rand.Next(175, 351) / 1000.0;
                case 2:
                    return rand.Next(150, 301) / 1000.0;
                case 3:
                    return rand.Next(150, 251) / 1000.0;
                default:
                    return 0.0;
            }
        }

        /// <summary>
        /// Get the randomness level used to solve the problem
        /// </summary>
        /// <returns>The randomness level</returns>
        public int GetRandomnessLevel()
        {
            return randLevel;
        }

        /// <summary>
        /// Returns the cost at the critical move
        /// </summary>
        /// <returns></returns>
        public int GetCostAtCriticalMove()
        {
            return minCriticalCost;
        }
    }
}
